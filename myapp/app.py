from flask import Flask
from flask_bootstrap import Bootstrap
import os.path

app = Flask(__name__)

app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)

def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),p
        )
    )

from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = (
    'sqlite:///'+mkpath('../album.db')
)
db = SQLAlchemy(app)

app.config["IMAGE_UPLOADS"] = mkpath('static/images')
app.config['SECRET_KEY'] = "d7e9b50f-d2b6-4366-bd29-2b01f82bf4d3"

from flask_login import LoginManager
login_manager = LoginManager(app)
login_manager.login_view = "login"
