import click
from .app import app, db

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    """
    Creates the tables and populates them with date.
    """
    #creation de toutes les tables
    db.create_all()

    #chargement de notre jeu de données
    import yaml
    songs = yaml.load(open(filename), Loader=yaml.FullLoader)
    artists = {}
    songss = {}
    genres = set()

    #import des modèles
    from .models import Artist, Song, Genre

    #première passe: création de tous les artistes
    for b in songs:
        a = b["artist"]
        if a not in artists:
            o = Artist(name=a)
            db.session.add(o)
            artists[a] = o
        db.session.add(o)
    db.session.commit()

    # deuxième passe: création de tous les sons
    for b in songs:
        a = artists[b["artist"]]
        t = b["title"]
        if t not in songss:
            o = Song(releaseYear  = b["releaseYear"],
                    title  = b["title"],
                    parent = b["parent"],
                    img    = b["img"],
                    spotify= b["spotify"],
                    artist_id = a.id)
            db.session.add(o)
            songss[t] = o
        db.session.add(o)
    db.session.commit()

    # troisième passe: création de tous les genres
    for b in songs:
        s = songss[b["title"]]
        for g in b["genre"]:
            o = Genre(name = g,
                        song_id = s.id)
            db.session.add(o)
        db.session.add(o)
    db.session.commit()

@app.cli.command()
def syncdb():
    """Creates all missing tables."""
    db.create_all()


@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    """Adds a new user."""
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(username=username, password=m.hexdigest())
    db.session.add(u)
    db.session.commit()


@app.cli.command()
@click.argument('username')
@click.argument('password')
def passwd(username, password):
    """Change the password of the user."""
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User.query.get(username)
    u.password = m.hexdigest()
    db.session.commit()
