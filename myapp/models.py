from .app import db, login_manager, app
from flask_login import UserMixin
from werkzeug.utils import secure_filename
import os.path
from hashlib import md5

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)

association_song_playlist = db.Table('association_song_playlist', db.metadata,
    db.Column('playlist_id', db.Integer, db.ForeignKey('playlist.id')),
    db.Column('song_id', db.Integer, db.ForeignKey('song.id'))
)

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key = True)
    password = db.Column(db.String(64))

    def get_id(self):
        return self.username

    def __repr__(self):
        return "%s" % (self.username)

    def avatar(self, size):
        digest = md5(self.username.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

class Playlist(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    songs = db.relationship('Song', secondary=association_song_playlist, backref=db.backref("playlists", lazy="dynamic"))
    user_name = db.Column(db.Integer, db.ForeignKey("user.username"))
    user = db.relationship("User", backref=db.backref("playlists", lazy="dynamic"))


class Artist(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return "%s" % (self.name)

class Song(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    releaseYear = db.Column(db.Integer)
    title = db.Column(db.String(200))
    parent = db.Column(db.String(200))
    img = db.Column(db.String(200))
    spotify = db.Column(db.String(200))
    artist_id = db.Column(db.Integer, db.ForeignKey("artist.id"))
    artist = db.relationship("Artist", backref=db.backref("songs", lazy="dynamic"))


    def __repr__(self):
        return "<Song (%d) %s >" % (self.id, self.title)

class Genre(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    song_id = db.Column(db.Integer, db.ForeignKey("song.id"))
    song = db.relationship("Song", backref=db.backref("genres", lazy="dynamic"))

    def __repr__(self):
        return "<Genre (%d) %s>" % (self.id, self.name)


def get_sample():
    return Song.query.limit(10).all()

def get_all():
    return Song.query.all()

def get_song(id):
    return Song.query.get(id)

def get_songs_artist(id):
    return Artist.query.filter(Artist.id == id).one().songs.all()

def get_artists():
    return Artist.query.all()

def get_artist(id):
    return Artist.query.get(id)

def get_artist_by_name(name):
    try:
        res = Artist.query.filter(Artist.name == name).one().id
    except:
        res = None
    return res

def get_artist_name(id):
    return Artist.query.get(id).name

def get_genres():
    return set([x.name for x in Genre.query.all()])

def get_genre(id):
    return Genre.query.get_or_404(id)

def songs_genre(name):
    genre = Genre.query.all()
    list_song_id = []
    for g in genre:
        if g.name == name:
            list_song_id.append(g.song_id)
    songs = Song.query.all()
    list_song = []
    for s in songs:
        for id in list_song_id:
            if s.id == id:
                list_song.append(s)
    return list_song

def img(data):
    fimg = data
    filename = secure_filename(fimg.filename)
    fimg.save(os.path.join(
        app.config["IMAGE_UPLOADS"], filename
    ))
    return filename

def taille_card_deck(contenu):
    taille = len(contenu)
    if taille > 5:
        taille = 5
    width = taille*20
    return width

def choix_recherche(choix, requete):
    res = None
    if choix == "Albums":
        res = get_songs_search(requete)
    elif choix == "Artistes":
        res = get_artists_search(requete)
    elif choix == "Genres":
        res = get_genres_search(requete)
    elif choix == "Année":
        res = get_annee_search(requete)
    return res

def get_songs_search(requete):
    search = "%{}%".format(requete)
    return Song.query.filter(Song.title.like(search)).all()

def get_artists_search(requete):
    search = "%{}%".format(requete)
    return Artist.query.filter(Artist.name.like(search)).all()

def get_genres_search(requete):
    search = "%{}%".format(requete)
    return set([x.name for x in Genre.query.filter(Genre.name.like(search)).all()])

def get_annee_search(requete):
    return Song.query.filter(Song.releaseYear.endswith(requete)).all()