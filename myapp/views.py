from .app import app, mkpath
from flask import render_template, redirect, url_for, flash, request
from flask_wtf import FlaskForm
from flask_login import login_user, current_user, logout_user, login_required
from wtforms import HiddenField, StringField, PasswordField, BooleanField, SubmitField, TextAreaField, SelectMultipleField
from flask_wtf.file import FileField
from wtforms.validators import ValidationError, DataRequired, EqualTo
from werkzeug.utils import secure_filename
from hashlib import sha256
from .models import *
from flask_login import login_required
from flask import request
from .commands import newuser
import os.path
# import Image


class ArtistForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom', validators=[DataRequired()])

class AlbumForm(FlaskForm):
    id = HiddenField('id')
    releaseYear = StringField('Année', validators=[DataRequired()])
    title = StringField('Titre', validators=[DataRequired()])
    parent = StringField('Parent', validators=[DataRequired()])
    img = FileField('Image')
    spotify = StringField('Lien Spotify',validators=[DataRequired()])
    artist = StringField('Artiste', validators=[DataRequired()])

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None

class CompteForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password', "Les mots de passes sont différents")])

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Veuillez utiliser un nom d'utilisateur différent ")

class PlaylistForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])

class PlaylistAddForm(FlaskForm):
    playlists = SelectMultipleField('Playslists', choices=[])


@app.route("/")
def home(message = None):
    return render_template(
        "home.html",
        title = "Accueil",
        songs = get_sample(),
        message = message,
        genres = get_genres(),
        home_active = "active"
    )

@app.route("/songs")
def songs():
    taille = len(get_all())
    if taille > 5:
        taille = 5
    width = taille*20
    return render_template(
        "songs.html",
        title="Les albums",
        songs=get_all(),
        genres = get_genres(),
        songs_active = "active",
    )

@app.route("/song/<int:id>", methods=("GET", "POST"))
def song(id):
    song = get_song(id)
    if current_user.is_authenticated:
        playlists = current_user.playlists
        playlists_name = []
        for p in playlists:
            playlists_name.append((p.name.lower(),p.name))
        f = PlaylistAddForm()
        f.playlists.choices = playlists_name
        if f.validate_on_submit():
            print(f.playlists.data)
            for p in f.playlists.data:
                playlist = Playlist.query.filter(Playlist.name==p.capitalize()).first()
                song = get_song(id)
                playlist.songs.append(song)
                song.playlists.append(playlist)
                db.session.commit()
                flash("Ajout réussi")
                return redirect(url_for('song', id = id))

        return render_template(
            "song.html",
            title = song.title,
            song = song,
            genres = get_genres(),
            songs_active = "active",
            form = f
        )
    else:
        return render_template(
            "song.html",
            title = song.title,
            song = song,
            genres = get_genres(),
            songs_active = "active"
        )


@app.route("/delete/album/<int:id>")
@login_required
def delete_album(id):
    a = get_song(id)
    artist = a.artist_id
    print(artist)
    db.session.delete(a)
    db.session.commit()
    return redirect(url_for('artist', id = artist))

@app.route("/edit/album/<int:id>")
@login_required
def edit_album(id):
    a = get_song(id)
    artist = get_artist_name(a.artist_id)
    f = AlbumForm(id = a.id, releaseYear=a.releaseYear, title=a.title, parent=a.parent, spotify = a.spotify, img=a.img, artist=artist)
    return render_template(
        "edit-album.html",
        album = a, form = f
    )

@app.route("/save/album/<int:appel>", methods=("POST", ))
def save_album(appel):
    a = None
    f = AlbumForm()
    if f.validate_on_submit():
        artist_id = get_artist_by_name(f.artist.data)
        if appel == 0:
            id = int(f.id.data)
            a = get_song(id)
            a.title = f.title.data
            a.releaseYear = f.releaseYear.data
            a.parent = f.parent.data
            a.spotify = f.spotify.data
            a.artist_id = artist_id
            if f.img.data:
                a.img = img(f.img.data)
        elif appel == 1:
            if not artist_id:   #si l'artiste n'existe pas, on le créé
                artist = Artist(name = f.artist.data)
                db.session.add(artist)
                db.session.commit()
                artist_id = artist.id
            a = Song(releaseYear = f.releaseYear.data,
                    title = f.title.data,
                    parent = f.parent.data,
                    img = img(f.img.data),
                    spotify = f.spotify.data,
                    artist_id = artist_id)
            db.session.add(a)
        db.session.commit()
        return redirect(url_for('song', id=a.id))
    return render_template(
        "new-song.html",
        form = f
    )

@app.route("/new/album/")
def new_album():
    f = AlbumForm()
    return render_template(
        "new-song.html",
        title = "Ajouter un album",
        form = f,
        genres = get_genres(),
        ajout_active = "active"
    )


@app.route("/artists")
def artists():
    return render_template(
        "artists.html",
        title = "Les artistes",
        artists = get_artists(),
        genres = get_genres(),
        artists_active = "active"
    )

@app.route("/artist/<int:id>")
def artist(id):
    artist = get_artist(id)
    return render_template(
        "artist.html",
        title = artist.name,
        artist = artist,
        artist_songs = get_songs_artist(id),
        genres = get_genres(),
        artists_active = "active"
    )

@app.route("/delete/artist/<int:id>")
@login_required
def delete_artist(id):
    a = get_artist(id)
    db.session.delete(a)
    db.session.commit()
    return redirect(url_for('artists'))

@app.route("/edit/artist/<int:id>")
@login_required
def edit_artist(id):
    a = get_artist(id)
    f = ArtistForm(id = a.id, name=a.name)
    return render_template(
        "edit-artist.html",
        artist = a, form = f, genres = get_genres()
    )

@app.route("/save/artist/<int:appel>", methods=("POST", ))
def save_artist(appel):
    a = None
    f = ArtistForm()
    if f.validate_on_submit():
        if appel == 0:
            id = int(f.id.data)
            a = get_artist(id)
            a.name = f.name.data
        elif appel == 1:
            a = Artist(name = f.name.data)
            db.session.add(a)
        db.session.commit()
        return redirect(url_for('artist', id=a.id))
    a = get_artist(int(f.id.data))
    return render_template(
        "edit-artist.html",
        artist=a, form=f
    )

@app.route("/new/artist/")
def new_artist():
    f = ArtistForm()
    return render_template(
        "new-artist.html",
        title = "Ajouter un artiste",
        form = f,
        genres = get_genres(),
        ajout_active = "active"
    )


@app.route("/genres/<string:name>")
def genre(name):
    taille = len(songs_genre(name))
    if taille > 5:
        taille = 5
    width = taille*20
    return render_template(
        "genre.html",
        title = name,
        genres = get_genres(),
        songs = songs_genre(name),
        genre_name = name,
        width = width,
        genres_active = "active"
    )

@app.route("/login/", methods=("GET", "POST",))
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            message = "Vous êtes connecté en tant que " + user.username
            flash(message)
            login_user(user)
            next = f.next.data
            return redirect(next) if next else redirect(url_for('home'))
        else:
            err = "Identifiant ou mot de passe incorrect"
            return render_template("login.html",title = "Se connecter", form = f, alert=err)
    return render_template(
        "login.html",
        title = "Se connecter",
        form = f,
        genres = get_genres()
    )

@app.route("/logout/")
def logout():
    logout_user()
    flash("Vous avez été deconnecté !")
    return redirect(url_for('home'))

@app.route("/new/compte", methods=("GET","POST",))
def new_compte():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    f = CompteForm()
    if f.validate_on_submit():
        m = sha256()
        m.update(f.password.data.encode())
        u = User(username=f.username.data, password=m.hexdigest())
        db.session.add(u)
        db.session.commit()
        flash("Compte créé")
        return redirect(url_for('login'))
    return render_template(
        'new-compte.html',
        form = f,
        genres = get_genres()
    )

@app.route("/user/<string:username>")
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    if user == current_user:
        return render_template(
            'user.html',
            title = "Votre compte",
            user = user,
            genres = get_genres()
        )
    else:
        return redirect(url_for('home'))

@app.route("/new/playlist", methods=("GET","POST",))
def new_playlist():
    f = PlaylistForm()
    if f.validate_on_submit():
        p = Playlist(name = f.name.data.capitalize() )
        current_user.playlists.append(p)
        db.session.add(p)
        db.session.commit()
        flash('Playlist créee')
        return redirect(url_for('user', username = current_user.username))
    return render_template(
        "new_playlist.html",
        form = f,
        genres = get_genres()
    )

@app.route("/playlist/<int:id>")
def playlist(id):
    playlist = Playlist.query.get(id)
    taille = len(playlist.songs)
    if taille > 5:
        taille = 5
    width = taille*20    
    return render_template(
        "playlist.html",
        title = "Playlist: %s" %playlist.name,
        playlist = playlist,
        width = width,
        genres = get_genres()
    )
    
@app.route("/delete/playlist/<int:id>")
def delete_playlist(id):
    p = Playlist.query.get(id)
    current_user.playlists.remove(p)
    db.session.commit()
    flash("Playlist supprimée")
    user = User.query.filter_by(username=current_user.username).first_or_404()
    return render_template(
        "user.html",
        user = user,
        genres = get_genres()

    )

@app.route("/search", methods = ['POST', 'GET'])
def search():
    form = request.form
    requete = form['rq']
    choix = form['select']
    contenu = choix_recherche(choix, requete)
    width = taille_card_deck(contenu)
    return render_template(
        "search.html",
        title = "Résultats d'%s pour  '%s'." %(choix.lower(), requete),
        contenu = contenu,
        choix = choix,
        width = width,
        genres = get_genres()
    )
